#
# pro_monitoring_sensor_example
#
# This file contains the sensor definition for the example sensor

# Dependencies
#    - Requires: edg-fabricMonitoring-agent >= 2.12.2-1
#
template pro_monitoring_sensor_example;

# definition
"/system/monitoring/sensor/example/cmdline" = "/usr/bin/perl /usr/libexec/sensors/lemon-sensor.pl Example";

# additional protocols the sensor supports
"/system/monitoring/sensor/example/supports" = "CFG SOD CHK VER";

# classes
"/system/monitoring/sensor/example/class" = list(

   	nlist("name", "example.uptime",
          	"descr", "report the uptime of the machine in seconds",
	      	"field", list(
			nlist("name", "Uptime", "format", "%ld", "scale", 1.0, "unit", "seconds"),)
       	),

	nlist("name", "example.http",
		"descr", "reports the http response code for a given url and service",
		"field", list(
			nlist("name", "ResponseCode", "format", "%ld", "descr", "The HTTP response code"),
	),

	nlist("name", "example.multi",
		"descr", "demonstrates multi-values and multi rowed metrics",
		"field", list(
			nlist("name", "Mountpoint",        "format", "%50s", "primary", true),
			nlist("name", "PartitionSize",     "format", "%ld", "scale", 1048576.0, "unit", "bytes"),
			nlist("name", "PartitionUsed",     "format", "%ld", "scale", 1048576.0, "unit", "bytes"),
		   	nlist("name", "PartitionUsedPerc", "format", "%ld"),)	
	),
);
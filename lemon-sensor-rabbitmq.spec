# Specfile
Summary:     Lemon sensor rabbitmq
Name:        lemon-sensor-rabbitmq
Version:     1.1
Release:     1
License:     GPL
Group:       System Environment/Base
BuildRoot:   %{_tmppath}/%{name}-root
BuildArch:   noarch
Source:      %{name}-%{version}.tar.gz

Requires:	 python-httplib2

%description
This package contains the lemon sensor for monitoring rabbitmq

%prep
%setup -n %{name}-%{version}

%install
rm -rf $RPM_BUILD_ROOT
make install prefix=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT
rm -rf $RPM_BUILD_DIR/%{name}-%{version}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING
%doc docs/*.html docs/tpl/*.tpl

%config /usr/libexec/sensors/*

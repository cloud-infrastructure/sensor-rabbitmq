#! /usr/bin/env python

# imports
import httplib2
import json

# the sensor API interface
from sensorAPI import *

# register package name and version
registerVersion("lemon-sensor-rabbitmq", "1.0.1-2")

# register the sensor metric classes
registerMetric("rabbitmq.partition", "Reports if the rabbitmq cluster is partitioned", "sensor-rabbitmq.Partition")
registerMetric("rabbitmq.message", "Reports the global number of messages currently queued", "sensor-rabbitmq.Message")
registerMetric("rabbitmq.messageRate", "Reports the global message rate", "sensor-rabbitmq.MessageRate")

class Message(Metric):
    '''
    Checks a rabbitmq cluster and returns the total number of message in the queues


    Arguments accepted:
        host      : default 127.0.0.1
        port      : default 15672
        auth_file : default None
    '''

    # the function 'sample' must be overwritten
    def sample(self):

        host = '127.0.0.1'
        if 'host' in self.parameters:
            host = self.parameters['host']

        port = '15672'
        if 'port' in self.parameters:
            port = self.parameters['port']

        h = httplib2.Http(".cache")

        if 'auth_file' in self.parameters:
            with open (self.parameters['auth_file'], "r") as auth_file:
                h.add_credentials(auth_file.readline().strip(), auth_file.readline().strip())

        try:
            (resp_headers, response) = h.request("http://%s:%s/api/overview" % (host,port), "GET", headers={'content-type':'application/json'})
        except Exception, e:
            self.log(SENSOR_LOG_ERROR, str(e))
            return -1

        try:
            data = json.loads(response)
            messages = data['queue_totals']['messages']
        except KeyError as e:
            # There are no messages
            messages = 0

        self.storeSample01(messages)
        return 0

class Partition(Metric):
    '''
    Checks a rabbitmq cluster and returns the number of partitions


    Arguments accepted:
        host      : default 127.0.0.1
        port      : default 15672
        auth_file : default None
    '''

    # the function 'sample' must be overwritten
    def sample(self):

        host = '127.0.0.1'
        if 'host' in self.parameters:
            host = self.parameters['host']

        port = '15672'
        if 'port' in self.parameters:
            port = self.parameters['port']

        h = httplib2.Http(".cache")

        if 'auth_file' in self.parameters:
            with open (self.parameters['auth_file'], "r") as auth_file:
                h.add_credentials(auth_file.readline().strip(), auth_file.readline().strip())

        try:
            (resp_headers, response) = h.request("http://%s:%s/api/nodes" % (host,port), "GET", headers={'content-type':'application/json'})
        except Exception, e:
            self.log(SENSOR_LOG_ERROR, str(e))
            return -1

        nodes = json.loads(response)

        partitions = 0
        for node in nodes:
            partitions += len( node['partitions'] )

        self.storeSample01(partitions)

        return 0

class MessageRate(Metric):
    '''
    Checks a rabbitmq cluster and returns various message rates


    Arguments accepted:
        host      : default 127.0.0.1
        port      : default 15672
        auth_file : default None
    '''

    # the function 'sample' must be overwritten
    def sample(self):

        host = '127.0.0.1'
        if 'host' in self.parameters:
            host = self.parameters['host']

        port = '15672'
        if 'port' in self.parameters:
            port = self.parameters['port']

        h = httplib2.Http(".cache")

        if 'auth_file' in self.parameters:
            with open (self.parameters['auth_file'], "r") as auth_file:
                h.add_credentials(auth_file.readline().strip(), auth_file.readline().strip())

        try:
            (resp_headers, response) = h.request("http://%s:%s/api/overview" % (host,port), "GET", headers={'content-type':'application/json'})
        except Exception, e:
            self.log(SENSOR_LOG_ERROR, str(e))
            return -1

        data = json.loads(response)
        publish_rate     = float(data['message_stats']['publish_details']['rate'])
        ack_rate         = float(data['message_stats']['ack_details']['rate'])
        deliver_get_rate = float(data['message_stats']['deliver_get_details']['rate'])
        redeliver_rate   = float(data['message_stats']['redeliver_details']['rate'])
        deliver_rate     = float(data['message_stats']['deliver_details']['rate'])

        value = "%d %d %d %d %d" % (publish_rate, ack_rate, deliver_get_rate, redeliver_rate, deliver_rate)

        self.storeSample01(value)

        return 0
